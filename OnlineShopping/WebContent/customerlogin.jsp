<%@ page language="java" contentType="text/html; charset=ISO-8859-1"
    pageEncoding="ISO-8859-1"%>

<!DOCTYPE html>
<html>
<head>
	<title>Register Or Login</title>
	<link rel="stylesheet" type="text/css" href="insert.css">
	<script type="text/javascript">
		function validate(){
			var id = document.forms["Insert"]["Product Id"];
			var email = document.forms["registerform"]["email"];
			var password = document.forms["registerform"]["password"];
			var confirmpassword = document.forms["registerform"]["confirmpassword"];
			var malegender = document.forms["registerform"]["malegender"];
			var femalegender = document.forms["registerform"]["femalegender"]
			if(id.value == ""){
				window.alert("Enter Product Id");
				name.focus();
				return false;
			}
			if(email.value=="")
			{
				window.alert("Enter an email");
				email.focus();
				return false;
			}
			if(email.value.indexOf("@",0)<1){
				window.alert("Enter a valid email address");
				email.focus();
				return false;
			}
			if(password.value==""){
				window.alert("Enter a password");
				password.focus();
				return false;
			}
			if(confirmpassword.value != password.value){
				window.alert("Password and confirmpassword should match");
				confirmpassword.focus();
				return false;
			}
			if(! (malegender.checked || femalegender.checked)){
				window.alert("Check atleast one value");
				return false;
			}
			return true;
		}
	</script>
</head>
<body>

<div class="navbar">
	<a  href=" " style="margin-right: 100px;font-size: 14px;font-family: times new roman;">
		
</div>

	<div id="registerorlogin">
		<h2 style="color: white;margin-left: 200px;margin-top:10px;position: absolute;">Login Page</h2>
		<div id="register">
			<div>
			<form action="CustomerRegistration" method="post">
				<input type="text" name="mail_id" placeholder="E-Mail" class="input" style="margin-top: 40px;" required>
				<input type="text" name="mobile_number" placeholder="Phone Number" class="input" style="margin-top: 90px;" required>
				<input type="password" name="password" placeholder="Password" class="input" style="margin-top: 140px;" required>
				<input type="text" name="address" placeholder="Address" class="input" style="margin-top: 190px;" required>
				
				
					
				
				<input type="submit" value="Register" style="width: 95px; height: 30px; margin-top: 50px; margin-left: 450px;background-color:#1a75ff;color:white;"><br><br>
				<a href="login.jsp" style="color:white;margin-left:450px;">Login</a>
				</form>
			</div>
		</div>
		
	</div>
</body>
</html>