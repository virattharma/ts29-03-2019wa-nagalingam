<%@ page language="java" contentType="text/html; charset=ISO-8859-1"
    pageEncoding="ISO-8859-1"%>

<!DOCTYPE html>
<html>
<head>
	<title>RegisterOrLogin</title>
	<link rel="stylesheet" type="text/css" href="insert.css">
	<script type="text/javascript">
		function validate(){
			var id = document.forms["Insert"]["Product Id"];
			var email = document.forms["registerform"]["email"];
			var password = document.forms["registerform"]["password"];
			var confirmpassword = document.forms["registerform"]["confirmpassword"];
			var malegender = document.forms["registerform"]["malegender"];
			var femalegender = document.forms["registerform"]["femalegender"]
			if(id.value == ""){
				window.alert("Enter Product Id");
				name.focus();
				return false;
			}
			if(email.value=="")
			{
				window.alert("Enter an email");
				email.focus();
				return false;
			}
			if(email.value.indexOf("@",0)<1){
				window.alert("Enter a valid email address");
				email.focus();
				return false;
			}
			if(password.value==""){
				window.alert("Enter a password");
				password.focus();
				return false;
			}
			if(confirmpassword.value != password.value){
				window.alert("Password and confirmpassword should match");
				confirmpassword.focus();
				return false;
			}
			if(! (malegender.checked || femalegender.checked)){
				window.alert("Check atleast one value");
				return false;
			}
			return true;
		}
	</script>
</head>
<body>
	<div id="registerorlogin">
		<h2 style="color: white;margin-left: 200px;margin-top:10px;position: absolute;">Insert Product</h2>
		<div id="register">
			<div>
			<form name="Insert" action="Insertion" method="post" enctype="multipart/form-data">
				<input type="text" name="Product Id" placeholder="Product Id" class="input" style="margin-top: 40px;">
				<input type="text" name="Product Name" placeholder="Product Name" class="input" style="margin-top: 90px;">
				<input type="text" name="Category Id" placeholder="Category Id" class="input" style="margin-top: 140px;">
				<input type="text" name="Manufacture Id" placeholder="Manufacture Id" class="input" style="margin-top: 190px;">
				<input type="text" name="Specifications" placeholder="Specifications" class="input" style="margin-top: 240px;">
				<input type="text" name="Quantity" placeholder="Quantity" class="input" style="margin-top: 290px;">
				<input type="text" name="Price" placeholder="Price" class="input" style="margin-top: 340px;">
				
					<img src="placeholderImage.svg" style="height: 220px;width: 250px;margin-top: 30px;margin-left: 390px;">
					<input type="file" name="ProductImage" style="margin-left: 440px;margin-top: 20px;"/>
				
				<input type="submit" value="Insert" style="width: 95px; height: 30px; margin-top: 50px; margin-left: 450px;">
				</form>
			</div>
		</div>
		
	</div>
</body>
</html>