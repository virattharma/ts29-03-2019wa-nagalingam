package Bean;

public class RegisterBean{
	private String mail_id;
	private String mobile_number;
	private String password;
	private String address;
	
	public String getMailid() {
		return mail_id;
	}
	public void setMailid(String mailid) {
		this.mail_id = mailid;
	}
	public String getMobilenumber() {
		return mobile_number;
	}
	public void setMobilenumber(String mobilenumber) {
		this.mobile_number = mobilenumber;
	}
	public String getPassword() {
		return password;
	}
	public void setPassword(String password) {
		this.password = password;
	}
	public String getAddress() {
		return address;
	}
	public void setAddress(String address) {
		this.address = address;
	}
	
}