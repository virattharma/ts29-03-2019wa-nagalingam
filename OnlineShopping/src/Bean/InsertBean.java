package Bean;

import java.sql.Blob;

public class InsertBean {
	String product_id;
	String product_name;
	int category_id;
	int manufacture_id;
	String Specificaions;
	int quantity;
	float price;
	public String getProduct_id() {
		return product_id;
	}
	public void setProduct_id(String product_id) {
		this.product_id = product_id;
	}
	public String getProduct_name() {
		return product_name;
	}
	public void setProduct_name(String product_name) {
		this.product_name = product_name;
	}
	public int getCategory_id() {
		return category_id;
	}
	public void setCategory_id(int category_id) {
		this.category_id = category_id;
	}
	public int getManufacture_id() {
		return manufacture_id;
	}
	public void setManufacture_id(int manufacture_id) {
		this.manufacture_id = manufacture_id;
	}
	public String getSpecificaions() {
		return Specificaions;
	}
	public void setSpecificaions(String specificaions) {
		Specificaions = specificaions;
	}
	public int getQuantity() {
		return quantity;
	}
	public void setQuantity(int quantity) {
		this.quantity = quantity;
	}
	public float getPrice() {
		return price;
	}
	public void setPrice(float price) {
		this.price = price;
	}
	
	
}
