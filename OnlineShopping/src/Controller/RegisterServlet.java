package Controller;
import java.io.IOException;
import java.io.PrintWriter;

import javax.servlet.ServletException;
import javax.servlet.annotation.WebServlet;
import javax.servlet.http.HttpServlet;
import javax.servlet.http.HttpServletRequest;
import javax.servlet.http.HttpServletResponse;

import Bean.RegisterBean;
import Dao.RegisterDao;
 
public class RegisterServlet extends HttpServlet {
	private static final long serialVersionUID = 1L;
     
	protected void doPost(HttpServletRequest request, HttpServletResponse response) throws ServletException, IOException {
		 String mailid=request.getParameter("mail_id");
		 String mobilenumber=request.getParameter("mobile_number");
		 String password=request.getParameter("password");
		 String address=request.getParameter("address");
		 
		 
		 
		 //System.out.println("username from servlet"+username);
		 RegisterBean insert=new RegisterBean();
		 
		 insert.setMailid(mailid); 
		 insert.setMobilenumber(mobilenumber); 
		 insert.setPassword(password);  
		 insert.setAddress(address); 
		 
		  
		 RegisterDao insertDao = new RegisterDao(); 
		 insertDao.InsertData(insert);
		 
		 response.setContentType("text/html");
		 PrintWriter writer =  response.getWriter();
		 writer.println("<h1 style=\"color:Tomato;\"> Registered Succesfully </h1>");	           
		 writer.close();
	  
	}

}

