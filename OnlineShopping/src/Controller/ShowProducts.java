package Controller;

import java.io.IOException;
import java.util.ArrayList;

import javax.servlet.ServletException;
import javax.servlet.annotation.WebServlet;
import javax.servlet.http.HttpServlet;
import javax.servlet.http.HttpServletRequest;
import javax.servlet.http.HttpServletResponse;
import javax.servlet.http.HttpSession;

import Bean.ProductBean;
import Dao.NumberOfProductsDao;
import Dao.ProductDao;


@WebServlet("/ShowProducts")
public class ShowProducts extends HttpServlet {
	
	private static final long serialVersionUID = 1L;
	 
protected void doGet(HttpServletRequest request, HttpServletResponse response) throws ServletException, IOException {

	int category_id = Integer.parseInt(request.getParameter("category_id"));
	System.out.println(category_id);
ProductBean bean=new ProductBean();
bean.setCategory_id(category_id);
ProductDao dao=new ProductDao();
ArrayList<Object> a = dao.getProductDetails(bean);

NumberOfProductsDao countDao = new NumberOfProductsDao();
int numberOfProducts = countDao.getNumberOfProducts(bean);

HttpSession session = request.getSession();
session.setAttribute("numberOfProducts", numberOfProducts);
int count= 1;
int x = 0; //number of columns
while(count<=numberOfProducts) {
	session.setAttribute("productid"+Integer.toString(count), a.get(x));
	session.setAttribute("productname"+Integer.toString(count), a.get(x+1));
	session.setAttribute("specification"+Integer.toString(count), a.get(x+2));
	session.setAttribute("price"+Integer.toString(count), a.get(x+3));
	x = x + 4;
	count++;
}
request.getRequestDispatcher("/product.jsp").forward(request, response);;

}  

}
