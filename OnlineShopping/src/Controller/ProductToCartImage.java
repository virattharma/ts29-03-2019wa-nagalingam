package Controller;

import java.io.IOException;
import java.util.ArrayList;

import javax.servlet.ServletException;
import javax.servlet.annotation.WebServlet;
import javax.servlet.http.HttpServlet;
import javax.servlet.http.HttpServletRequest;
import javax.servlet.http.HttpServletResponse;
import javax.servlet.http.HttpSession;
import Dao.CartCountDao;
import Dao.CountDao;
import Dao.NumberOfProductsDao;


@WebServlet("/ProductToCartImage")
public class ProductToCartImage extends HttpServlet {
	private static final long serialVersionUID = 1L;
       
	protected void doGet(HttpServletRequest request, HttpServletResponse response) throws ServletException, IOException {
	CartCountDao dao=new CartCountDao();
	int a = dao.CartProducts();
	
	CountDao countDao = new CountDao();
	ArrayList<Object>numberOfProducts = countDao.CartProducts();
	
	HttpSession session = request.getSession();
	session.setAttribute("numberOfProducts", numberOfProducts);
	int count= 1;
	int x = 0; //number of columns
	while(count<=a) {
		session.setAttribute("productid"+Integer.toString(count), numberOfProducts.get(x));
		session.setAttribute("productname"+Integer.toString(count), numberOfProducts.get(x+1));
		session.setAttribute("specification"+Integer.toString(count), numberOfProducts.get(x+2));
		session.setAttribute("price"+Integer.toString(count),numberOfProducts.get(x+3));
		x = x + 4;
		count++;
	}
	request.getRequestDispatcher("/Cart.jsp").forward(request, response);;

	}  
}
 