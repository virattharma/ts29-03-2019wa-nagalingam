package Controller;

import java.io.IOException;
import java.io.InputStream;
import java.io.PrintWriter;
import java.sql.Blob;

import javax.servlet.ServletException;
import javax.servlet.annotation.MultipartConfig;
import javax.servlet.annotation.WebServlet;
import javax.servlet.http.HttpServlet;
import javax.servlet.http.HttpServletRequest;
import javax.servlet.http.HttpServletResponse;
import javax.servlet.http.Part;

import Bean.InsertBean;
import Dao.insertDao;
@MultipartConfig(maxFileSize = 20000000)
public class insertServlet extends HttpServlet {
	private static final long serialVersionUID = 1L;
       
	protected void doPost(HttpServletRequest request, HttpServletResponse response) throws ServletException, IOException {
		 String product_id=request.getParameter("Product Id");
		 String product_name=request.getParameter("Product Name");
		 int category_id=Integer.parseInt(request.getParameter("Category Id"));
		 int manufacture_id=Integer.parseInt(request.getParameter("Manufacture Id"));
		 String specifications=request.getParameter("Specifications");
		 int quantity=Integer.parseInt(request.getParameter("Quantity"));
		 float price=Float.parseFloat(request.getParameter("Price"));
	
		
		 
		 
		 System.out.println("Product from servlet"+product_name);
		 InsertBean insert=new InsertBean();
		 
		 insert.setProduct_id(product_id); 
		 insert.setProduct_name(product_name);
		 insert.setCategory_id(category_id);
		 insert.setManufacture_id(manufacture_id);
		 insert.setSpecificaions(specifications);
		 insert.setQuantity(quantity);
		 insert.setPrice(price);
		 
		  
		 insertDao insertDao = new insertDao(); 
		 insertDao.InsertData(insert);
		 
		 InputStream inputStream = null;
			Part imagePart = request.getPart("ProductImage");
			if (imagePart != null) {
	        inputStream = imagePart.getInputStream();
			}
			
	        insertDao PicInsert = new insertDao();
			PicInsert.insertImage(inputStream,product_id);
			
			response.sendRedirect("insert.jsp");
		 
		 response.setContentType("text/html");
		 PrintWriter writer =  response.getWriter();
		 writer.println("<h1 style=\"color:Tomato;\"> Inserted Succesfully </h1>");	           
		 writer.close();
	  
	}

}
