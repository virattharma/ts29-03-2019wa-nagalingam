package Controller;
import java.io.IOException;
import java.io.InputStream;
import java.io.PrintWriter;
import java.util.Date;
import javax.servlet.ServletException;
import javax.servlet.annotation.WebServlet;
import javax.servlet.http.HttpServlet;
import javax.servlet.http.HttpServletRequest;
import javax.servlet.http.HttpServletResponse;
import javax.servlet.http.HttpSession;
import javax.servlet.http.Part;
import Bean.BuyBean;


@WebServlet("/BuyServlet")
public class BuyServlet extends HttpServlet {
	private static final long serialVersionUID = 1L;    
	     protected void doPost(HttpServletRequest request, HttpServletResponse response) throws ServletException, IOException {
		 int NumberOfCartProducts=Integer.parseInt(request.getParameter("count")); 
		 System.out.println(NumberOfCartProducts);
		 HttpSession session = request.getSession();
		 float total = 0;
		 int total_quantity = 0;
	     for(int i=1;i<=NumberOfCartProducts;i++) {
			System.out.println(request.getParameter("quantity"+Integer.toString(i))); 
		 /*String product_name=request.getParameter("product_name"+Integer.toString(i));*/
		 float price=(float)session.getAttribute("price"+Integer.toString(i));
		 int quantity=Integer.parseInt(request.getParameter("quantity"+Integer.toString(i)));
		 total += price*quantity;
		 total_quantity += quantity;
		 }
		 session.setAttribute("total", total);
		 session.setAttribute("quan", total_quantity); 
		 
			
		 request.getRequestDispatcher("BuyNow.jsp").forward(request, response);
		 /*response.setContentType("text/html");
		 PrintWriter writer =  response.getWriter();
		 writer.println("<h1 style=\"color:Tomato;\"> Inserted Succesfully </h1>");	           
		 writer.close();*/
 
	}
}
