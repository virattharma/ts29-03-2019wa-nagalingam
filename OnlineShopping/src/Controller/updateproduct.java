package Controller;

import java.io.IOException;
import java.io.PrintWriter;

import javax.servlet.ServletException;
import javax.servlet.annotation.WebServlet;
import javax.servlet.http.HttpServlet;
import javax.servlet.http.HttpServletRequest;
import javax.servlet.http.HttpServletResponse;

import Bean.UpdateBean;
import Dao.updateDao;


public class updateproduct extends HttpServlet {
	private static final long serialVersionUID = 1L;
	protected void doPost(HttpServletRequest request, HttpServletResponse response) throws ServletException, IOException {
		 String product_name=request.getParameter("ProductName");
		 Float price=Float.parseFloat(request.getParameter("Price"))  ;
		 
		 
		 System.out.println("Product Name from servlet"+product_name);
		 System.out.println("Product Price from servlet"+price);
		 UpdateBean update=new UpdateBean();
		 
		 update.setProduct_name(product_name); 
		 update.setPrice(price);
		  
		 updateDao updateDao = new updateDao(); 
		 updateDao.update(update);
		 	 
		 response.setContentType("text/html");
		 PrintWriter writer =  response.getWriter();
		 writer.println("<h1 style=\"color:Tomato;\"> Update Succesfully </h1>");	           
		 writer.close();
	}

}
