package Controller;


import java.io.IOException;
import java.io.PrintWriter;

import javax.servlet.ServletException;
import javax.servlet.annotation.WebServlet;
import javax.servlet.http.HttpServlet;
import javax.servlet.http.HttpServletRequest;
import javax.servlet.http.HttpServletResponse;

import Bean.RemoveBean;
import Dao.RemoveDao;



public class RemoveServlet extends HttpServlet {
	private static final long serialVersionUID = 1L;
 
	 
	protected void doPost(HttpServletRequest request, HttpServletResponse response) throws ServletException, IOException {
		String product_name=request.getParameter("ProductName");
		System.out.println(request.getParameter("ProductName"));
		 System.out.println("Product Name from servlet"+product_name);
		 
		
		 

		 RemoveBean delete=new RemoveBean();
		 
		 delete.setProduct_name(product_name);
		 
		  
		
		  
		RemoveDao deleteDao = new RemoveDao(); 
		 deleteDao.delete(delete);
		 
		 response.setContentType("text/html");
		 PrintWriter writer =  response.getWriter();
		 writer.println("<h1 style=\"color:red;\"> deleted Succesfully </h1>");	           
		 writer.close();
	}
}
