package Controller;

import java.io.IOException;
import javax.servlet.ServletException;
import javax.servlet.annotation.WebServlet;
import javax.servlet.http.HttpServlet;
import javax.servlet.http.HttpServletRequest;
import javax.servlet.http.HttpServletResponse;
import Bean.LoginBean;
import Dao.LoginDao;


@WebServlet("/LoginServlet")
public class LoginServlet extends HttpServlet {
	private static final long serialVersionUID = 1L;

	protected void doPost(HttpServletRequest request, HttpServletResponse response) throws ServletException, IOException {
		 String mobilenumber=request.getParameter("mobile_number");
		 String password=request.getParameter("password");
		 System.out.println("hai");
		 LoginBean loginBean = new LoginBean();

		 loginBean.setMobile_number(mobilenumber); 
		 loginBean.setPassword(password);
       
		 LoginDao loginDao = new LoginDao();
		 

		 String userValidate = loginDao.authenticateUser(loginBean); 

		 if(userValidate.equals("SUCCESS")) 
		 {
		 request.setAttribute("mobile_number", mobilenumber); 
		 request.getRequestDispatcher("AfterLogin.jsp").forward(request, response);
		 }
		 else
		 {
		 request.setAttribute("errMessage", userValidate);
		 request.getRequestDispatcher("/index.jsp").forward(request, response);
		 }
}
}