package Controller;

import java.io.IOException;
import java.io.InputStream;
import java.io.PrintWriter;

import javax.servlet.ServletException;
import javax.servlet.annotation.MultipartConfig;
import javax.servlet.annotation.WebServlet;
import javax.servlet.http.HttpServlet;
import javax.servlet.http.HttpServletRequest;
import javax.servlet.http.HttpServletResponse;
import javax.servlet.http.Part;

import Bean.OrderBean;
import Dao.OrderDao;
import Dao.insertDao;
public class OrderServlet extends HttpServlet {
	private static final long serialVersionUID = 1L;
       
	protected void doPost(HttpServletRequest request, HttpServletResponse response) throws ServletException, IOException {
		 int quantity=Integer.parseInt(request.getParameter("quantity"));
		 
		 float total_amount=Float.parseFloat(request.getParameter("price"));
		
		
		 
		 
		
		 OrderBean insert=new OrderBean();
		 
		 
		 insert.setTotal_amount(total_amount);
		
		 insert.setQuantity(quantity);
		 
		 
		  
		 OrderDao insertDao = new OrderDao(); 
		 insertDao.InsertData(insert);
		 
		 
			
			response.sendRedirect("index.jsp");
		 
		 response.setContentType("text/html");
		 PrintWriter writer =  response.getWriter();
		 writer.println("<h1 style=\"color:Tomato;\"> Inserted Succesfully </h1>");	           
		 writer.close();
	  
	}

}
