package Controller;

import java.io.IOException;
import javax.servlet.ServletException;
import javax.servlet.annotation.WebServlet;
import javax.servlet.http.HttpServlet;
import javax.servlet.http.HttpServletRequest;
import javax.servlet.http.HttpServletResponse;

import Bean.RemoveCartBean;
import Dao.RemoveCartDao;


@WebServlet("/RemoveCartServlet")
public class RemoveCartServlet extends HttpServlet {
	private static final long serialVersionUID = 1L;
	protected void doPost(HttpServletRequest request, HttpServletResponse response) throws ServletException, IOException {
		String product_id=request.getParameter("ProductId");
		 System.out.println("Product Name from servlet"+product_id);
		 
		
		 

		 RemoveCartBean delete=new RemoveCartBean();
		 
		 delete.setProduct_id(product_id);  
		 RemoveCartDao deleteDao = new RemoveCartDao(); 
		 deleteDao.deleted(delete);  
		 /*request.getRequestDispatcher("Cart.jsp").forward(request, response);*/
		 response.sendRedirect("Retrieve");
	}
}
