package Dao;

import java.sql.Connection;
import java.sql.PreparedStatement;
import java.sql.ResultSet;
import Connection.ConnectionDb;

public class NumberOfCartProducts {
	public int getNumberOfCartProducts() {
		int numberOfProducts = 0;
		Connection con = ConnectionDb.createConnection();
		try {
		PreparedStatement stmt=con.prepareStatement("select count(*) from cart");
		ResultSet resultSet=stmt.executeQuery();
		while(resultSet.next()) {
			numberOfProducts = resultSet.getInt(1);
		}
		}
		catch(Exception numberException) {
			System.out.println(numberException);
		}
		return numberOfProducts;
}

}
