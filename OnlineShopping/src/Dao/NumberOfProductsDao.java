package Dao;

import java.sql.Connection;
import java.sql.PreparedStatement;
import java.sql.ResultSet;
import java.util.ArrayList;

import Bean.ProductBean;
import Connection.ConnectionDb;

public class NumberOfProductsDao {
	public int getNumberOfProducts(ProductBean bean) {
		int categoryid = bean.getCategory_id();
		int numberOfProducts = 0;
		Connection con = ConnectionDb.createConnection();
		try {
		PreparedStatement stmt=con.prepareStatement("select count(*) from product where category_id = (?)");
		stmt.setInt(1, categoryid);
		ResultSet resultSet=stmt.executeQuery();
		while(resultSet.next()) {
			numberOfProducts = resultSet.getInt(1);
		}
		}
		catch(Exception numberException) {
			System.out.println(numberException);
		}
		return numberOfProducts;
}
}
