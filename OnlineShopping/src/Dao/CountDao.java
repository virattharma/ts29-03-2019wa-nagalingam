package Dao;
import java.sql.Connection;
import java.sql.PreparedStatement;
import java.sql.ResultSet;
import java.sql.SQLException;
import java.util.ArrayList;
import Connection.ConnectionDb;

public class CountDao {
	public ArrayList<Object> CartProducts(){
		
		ArrayList<Object> a=new ArrayList<Object>();
		Connection con = ConnectionDb.createConnection();
	try {
	PreparedStatement stmt=con.prepareStatement("select product_name, specifications, price from cart inner join product on cart.product_id = product.product_id;");
	ResultSet resultSet=stmt.executeQuery();
	while(resultSet.next()) {
		a.add(resultSet.getString("product_name"));
		a.add(resultSet.getString("specifications"));
		a.add(resultSet.getFloat("price"));
		}
	}
			catch(SQLException e) {
				e.printStackTrace();
			}
	
	//another statement to collect count
	
	
			return a;
		
		}
}
