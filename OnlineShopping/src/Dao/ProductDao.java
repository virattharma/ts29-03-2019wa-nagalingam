package Dao;

import java.io.IOException;
import javax.servlet.ServletException;
import javax.servlet.annotation.WebServlet;
import javax.servlet.http.HttpServlet;
import javax.servlet.http.HttpServletRequest;
import javax.servlet.http.HttpServletResponse;

import Bean.ProductBean;
import Connection.ConnectionDb;

import java.sql.*;
import java.util.ArrayList;




public class ProductDao extends HttpServlet {
	private static final long serialVersionUID = 1L;
		public ArrayList<Object> getProductDetails(ProductBean bean){
			int categoryid = bean.getCategory_id();
			ArrayList<Object> a=new ArrayList<Object>();
			Connection con = ConnectionDb.createConnection();
		try {
		PreparedStatement stmt=con.prepareStatement("select product_id, product_name, specifications, price from product where category_id = (?)");
		stmt.setInt(1, categoryid);
		ResultSet resultSet=stmt.executeQuery();
		while(resultSet.next()) {
			a.add(resultSet.getString("product_Id")); 
			a.add(resultSet.getString("product_name"));
			a.add(resultSet.getString("specifications"));
			a.add(resultSet.getFloat("price"));
			}
		}
				catch(SQLException e) {
					e.printStackTrace();
				}
		
		//another statement to collect count
		
		
				return a;
			
			}
		}

