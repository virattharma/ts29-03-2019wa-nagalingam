package Dao;

import java.sql.Connection;
import java.sql.PreparedStatement;
import java.sql.ResultSet;
import java.sql.SQLException;
import java.util.ArrayList;

import Bean.RemoveCartBean;
import Connection.ConnectionDb;

public class RemoveCartDao {
	public void deleted(RemoveCartBean deletebean) {
		  String product_id=deletebean.getProduct_id();
		
			
			
		  
		  Connection con = null;
		  
		  
		  try {
			    con=ConnectionDb.createConnection();
				PreparedStatement deleteStatement = con.prepareStatement("delete from cart where product_id=(?)");
				deleteStatement.setString(1,product_id);
		
			
				
				int i = deleteStatement.executeUpdate();
				System.out.println(i+ "row deleted");
				
			}
			catch(Exception updateError) {
				System.out.println(updateError);
			} 
		}  
}
