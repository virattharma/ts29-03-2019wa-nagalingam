package Dao;
import java.sql.Connection;
import java.sql.ResultSet;
import java.sql.SQLException;
import java.sql.Statement;

import Bean.LoginBean;
import Connection.ConnectionDb;
public class LoginDao {
public String authenticateUser(LoginBean loginBean)
{

String mobileNumber = loginBean.getMobile_number();
String password = loginBean.getPassword();
Connection con = null;
Statement statement = null;
ResultSet resultSet = null;

String mobileNumberDB = "";
String passwordDB = "";

try
{
con = ConnectionDb.createConnection(); 
statement = con.createStatement(); 
resultSet = statement.executeQuery("select mobile_number,password from register");

while(resultSet.next()) 
{
 mobileNumberDB = resultSet.getString("mobile_number"); 
 passwordDB = resultSet.getString("password");

  if(mobileNumber.equals(mobileNumberDB) && password.equals(passwordDB))
  {
     return "SUCCESS"; 
  }
}
}
catch(SQLException e)
{
e.printStackTrace();
}
return "Invalid user credentials"; 
}
}
