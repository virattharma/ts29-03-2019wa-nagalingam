package Dao;

import java.sql.Connection;
import java.sql.PreparedStatement;

import Bean.UpdateBean;
import Connection.ConnectionDb;

public class updateDao {
	public void update(UpdateBean updatebean) {
		  String product_name=updatebean.getProduct_name();
		  float price=updatebean.getPrice();
		 
		  Connection con = null;
		  
		  try {
				con = ConnectionDb.createConnection();
				PreparedStatement updateStatement = con.prepareStatement("Update product set price = (?) where product_name = (?)");
				updateStatement.setFloat(1,price);
				updateStatement.setString(2,product_name);
			
				
				int i = updateStatement.executeUpdate();
				System.out.println(i+ "row updated");
			}
			catch(Exception updateError) {
				System.out.println(updateError);
			}
		  
		  
		  
		}
		  

}
