package Dao;

import java.io.InputStream;
import java.sql.Blob;
import java.sql.Connection;
import java.sql.PreparedStatement;
import java.sql.SQLException;

import Bean.InsertBean;
import Connection.ConnectionDb;

public class insertDao {
	public String InsertData(InsertBean insert) {
		String product_id=insert.getProduct_id();
		String product_name=insert.getProduct_name();
		int category_id=insert.getCategory_id(); 
		int manufacture_id=insert.getManufacture_id();
		String specifications=insert.getSpecificaions();
		int quantity=insert.getQuantity();
		float price=insert.getPrice();
		
		System.out.println("------------->"+product_name);
		Connection con = null;
	 
	 
		 
		try
		 {
		 con = ConnectionDb.createConnection(); 
		 PreparedStatement stmt=con.prepareStatement("insert into product(product_id, product_name, category_id, manufacture_id, specifications, quantity, price) values(?,?,?,?,?,?,?)");
		 stmt.setString(1,product_id);
		 stmt.setString(2,product_name);
		 stmt.setInt(3,category_id);
		 stmt.setInt(4,manufacture_id);
		 stmt.setString(5,specifications);
		 stmt.setInt(6,quantity);
		 stmt.setFloat(7,price);
	
		 	
		 int i=stmt.executeUpdate();  
			System.out.println(i+" record inserted");  
			  
			con.close();  
		 }
		 catch(SQLException e)
		 {
		 e.printStackTrace();
		 }
		 return "Invalid user credentials"; 
		 
		
		
	}

	
		public void insertImage(InputStream inputStream, String product_id) {
			try {
				Connection con = ConnectionDb.createConnection();
				System.out.println("Hai"); 
				PreparedStatement uploadPic = con.prepareStatement("update product set image=(?) where product_id = (?)");
				System.out.println("Hai image"); 
				if(inputStream!=null) {
					uploadPic.setBlob(1, inputStream);
					System.out.println("inside If"); 
				}
				uploadPic.setString(2, product_id);
				uploadPic.executeUpdate();
			}
			catch(Exception imageUploadError) {
				System.out.println(imageUploadError);
			}
			
		
	}

}
